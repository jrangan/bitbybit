const apiGetAll = async (url, updateData) => {
  return await fetch(url)
    .then((response) => response.json())
    .then((data) => updateData(data.todoList))
    .catch((error) => console.log(error));
};

const apiCreateTodo = async (url, todoName) => {
  const response = await fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      name: todoName,
    }),
  });

  const jsonData = await response.json();
  return { id: jsonData.id, name: jsonData.name, done: jsonData.complete };
};

const apiPatch = async (url, id, newValue, todoProperty) => {
  return await fetch(url + "/" + id, {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ [todoProperty]: newValue }),
  });
};

const apiDelete = async (url, id) => {
  return await fetch(url + "/" + id, {
    method: "DELETE",
  });
};

export { apiGetAll, apiCreateTodo, apiPatch, apiDelete };
