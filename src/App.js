import * as React from "react";
import CreateTask from "./CreateTask";
import UpdateTodo from "./UpdateTodo";
import TaskList from "./TaskList";
import { apiGetAll, apiCreateTodo, apiPatch, apiDelete } from "./api";
import "./App.css";

const App = () => {
  const [todoList, setTodoList] = React.useState([]);
  const [updatedTodoName, setUpdatedTodoName] = React.useState("Blank");

  const url = "http://localhost:5000/todo";

  React.useEffect(() => {
    apiGetAll(url, setTodoList);
  }, []);

  const addNewItemToTodoList = async (itemName) => {
    //API call Add to DB
    const newItem = await apiCreateTodo(url, itemName);
    console.log("ADDED");
    const updatedList = [...todoList, newItem];
    setTodoList(updatedList);
  };

  const removeItemFromTodoList = async (id) => {
    const updateList = todoList.filter((task) => task.id !== id);
    setTodoList(updateList);

    //API call Remove from DB
    await apiDelete(url, id);
    console.log("REMOVED");
  };

  const updateItemFromTodoList = async (id, updatedName) => {
    //const listCopy = JSON.parse(JSON.stringify(todoList));
    //listCopy[id].name = updatedName;
    const listCopy = todoList.map((task) => {
      if (task.id === id) {
        return { ...task, name: updatedName };
      }
      return { ...task };
    });
    setTodoList(listCopy);

    //API update name of todo in DB
    await apiPatch(url, id, updatedName, "name");
    console.log("UPDATED");
  };
  const strikeItemInTodoList = async (id) => {
    const updateList = JSON.parse(JSON.stringify(todoList));
    if (updateList[id].done === true) {
      updateList[id].done = false;

      //API set complete of item at id to false
      await apiPatch(url, id, false, "complete");
    } else {
      updateList[id].done = true;
      //API set complete of item at id to false
      await apiPatch(url, id, true, "complete");
    }
    setTodoList(updateList);
  };

  return (
    <div className="App">
      <header className="ToDoList">
        <h1>BIT BY BIT</h1>
      </header>
      <CreateTask updateTasks={addNewItemToTodoList} />
      <UpdateTodo updateName={setUpdatedTodoName} />
      <TaskList
        updatedName={updatedTodoName}
        updatedTaskName={updateItemFromTodoList}
        removeTask={removeItemFromTodoList}
        setComplete={strikeItemInTodoList}
        tasks={todoList}
      />
    </div>
  );
};

export default App;
